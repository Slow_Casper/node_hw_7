import React from 'react';

const Button = ({ text, action, ...props }) => {
  return (
    <button onClick={action} {...props}>{text}</button>
  );
};

export default Button;
