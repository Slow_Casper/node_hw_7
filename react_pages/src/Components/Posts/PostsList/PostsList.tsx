import React, { ReactElement } from 'react'
import PropTypes from 'prop-types';
import PostElement from '../PostElement/PostElement.tsx'

export interface Post {
  [key: string]: number | string
};

const PostsList = ({ posts, elementsPerPage }) => {
  return (
    <section 
      id='posts'
      style={{
        display: 'grid',
        gridTemplateColumns: `repeat(${elementsPerPage}, 1fr)`,
        gap: '20px',
      }}
    >
      {posts.length > 0 ? (
        posts.map((el: Post): ReactElement<any> => (
          <PostElement key={el.id} element={el} />
        ))
      ) : (
        <p>There is no posts yet</p>
      )}
    </section>
  )
}

export default PostsList

PostsList.propTypes = {
  posts: PropTypes.array.isRequired,
};
