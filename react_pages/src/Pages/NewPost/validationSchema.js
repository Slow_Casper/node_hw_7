import { object, string } from 'yup';

export const validationSchema = object({
    title: string().required('This field is required'),
    text: string().required('This field is required'),
});
