import React from 'react'
import { Route, Routes } from 'react-router'
import MainPage from './Pages/MainPage/MainPage.tsx'
import SinglePostPage from './Pages/SinglePostPage/SinglePostPage.tsx'
import NewEditPost from './Pages/NewPost/NewEditPost.jsx'

const AppRoutes = () => {
  return (
    <Routes>
        <Route index element={<MainPage />} />
        <Route path='/news/:newsId' element={<SinglePostPage />} />
        <Route path='/newpost' element={<NewEditPost />} />
        <Route path='/editpost/:newsId' element={<NewEditPost />} />
    </Routes>
  )
}

export default AppRoutes
