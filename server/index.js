const express = require('express');
const dotenv = require('dotenv');
const path = require('path');
const cors = require('cors');

const staticGet = require('./routes/staticGet');
const newsPosts = require('./routes/newsPosts');

dotenv.config();

const app = express();
const staticDirPath = path.resolve(__dirname, '../react_pages/build');

app.use(express.json());

app.use(express.static(staticDirPath));
app.use(cors({
    origin: process.env.REDIRECT_URL,
    methods: 'GET, POST, PUT, DELETE',
}));

app.use('/api/newsposts', newsPosts);

app.use('*', staticGet);

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => console.log(`server is running on port ${PORT}`));
