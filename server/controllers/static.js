const path = require('path');

const staticHTML = path.resolve(__dirname, '../react_pages/build/index.html');

exports.staticGet = (req, res) => {
    if(staticHTML) res.status(200).json(staticHTML);
    res.status(400).json({
        message: 'File is not found on server'
    })
};