const { getAll, create, getById, update, delPost } = require('../dal/newspostsService');

exports.getAllPosts = async (req, res) => await getAll(req.query, res);
exports.addNewPost = async (req, res) => await create(req.body, res);
exports.getSinglePost = async (req, res) => await getById(req.params.id, res);
exports.updatePost = async (req, res) => update(req.params.id, req.body, res);
exports.deletePost = async (req, res) => delPost(req.params.id, res);
