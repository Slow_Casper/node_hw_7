const express = require('express');
const { getAllPosts, getSinglePost, addNewPost, updatePost, deletePost } = require('../controllers/posts');
const router = express.Router();

router.get('/', getAllPosts);

router.post('/', addNewPost);

router.get('/:id', getSinglePost);

router.put('/:id', updatePost);

router.delete('/:id', deletePost);

module.exports = router;
